%%-----------------------------------------------------------------------%%
%  Authors: M.H.C. van Riel and M.A. Cloos
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 16
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
function [avgObjFcn, gradFcn, hessMultFcn] = objective(T1List, B1List, m0, seqParam, jacAutodiff)
%Calculate the objective function, averaged over all combinations of T1 and
%B1+. Optionally, provide a function for the gradient of the objective
%value as well, and a function that calculates the hessian-times-vector
%product.
%   Inputs:
%   T1List: 1xN list of T1 values to optimize for
%   B1List: 1xN list of B1 values to optimize for
%   m0: equilibrium magnetization, proportional to proton density
%   seqParam: sequence parameters used for fingerprint simulation
%   jacAutodiff: use automatic differentiation for the Jacobian or not
%
%   Outputs:
%   avgObjFcn: function that takes the flip angles as input and gives the
%       (averaged) objective value (the trace of the Cramér-Rao Bound) as
%       output
%   gradFcn: function that takes the flip angles as input and gives the
%       gradient of the objective value as output
%   hessMultFcn: function that takes the flip angles and another vector as
%       input, and gives the result of the multiplication of the Hessian
%       matrix with the given vector

% Parameter dimensions
nT1 = length(T1List);
nB1 = length(B1List);
nComb = nT1 * nB1;

% Make parameter combinations
T1Comb = repelem(T1List, nB1);
B1Comb = repmat(B1List, 1, nT1);

% Set up symbolic parameters
FAs_SX = casadi.SX.sym('FAs', seqParam.seqLength);
T1_SX = casadi.SX.sym('T1');
B1_SX = casadi.SX.sym('B1');

FAs_MX = casadi.MX.sym('FAs', seqParam.seqLength);

% Objective function for single parameter combination
obj_SX = trace(calc_CRLB(T1_SX, B1_SX, m0, FAs_SX, seqParam, jacAutodiff));
objFcn = casadi.Function('objective_function', {T1_SX, B1_SX, FAs_SX}, {obj_SX}, ...
    {'T1', 'B1', 'flip angles'}, {'objective value'});

% Average over all parameter combinations
objFcnMap = objFcn.map(nComb, 'thread', 4);
objAllComb = objFcnMap(T1Comb, B1Comb, FAs_MX);
avgObj = mean(objAllComb);
avgObjFcn = casadi.Function('avgerage_objective_function', {FAs_MX}, {avgObj}, ...
    {'flip angles'}, {'average objective value'});
avgObjFcn = returntypes('full', avgObjFcn);

% In case of two outputs, generate the gradient function as well
if nargout > 1
    grad = gradient(avgObj, FAs_MX);
    gradFcn = casadi.Function('gradient_function', {FAs_MX}, {grad}, ...
        {'flip angles'}, {'objective value gradient'});
    gradFcn = returntypes('full', gradFcn);
end

% In case of three outputs, generate the Hessian-mulitply-function as well
if nargout > 2
    % Create symoblic vector
    v_MX = casadi.MX.sym('vector', seqParam.seqLength);
    
    % Use forward mode AD to calculate the Jacobian-times-vector product
    gv = jtimes(avgObj, FAs_MX, v_MX);
    
    % Calculate the gradient of this to get the Hessian-times-vector
    % product
    Hv = gradient(gv, FAs_MX);
    
    hessMultFcn = casadi.Function('hessian_multiply_function', {FAs_MX, v_MX}, {Hv}, ...
        {'flip angles', 'vector'}, {'hessian-times-vector'});
    hessMultFcn = returntypes('full', hessMultFcn);
end

end

