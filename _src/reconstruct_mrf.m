%%-----------------------------------------------------------------------%%
%  Authors: M.H.C. van Riel and M.A. Cloos
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 14
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
function img = reconstruct_mrf(MrData, I_rx, trj)
%Reconstructs the data of the MR Fingerprinting sequence
%   Inputs:
%   MrData: data object
%   I_rx: Estimated receive sensitivities
%   trj: k-space trajectory, with fields k and w containing the k-space
%       coordinates and weights for every data point, respectively
%
%   Outputs:
%   img: dim.nEc x dim.nSl x dim.nRe x dim.nRe x dim.nSe array of the 
%       reconstructed images

% Extract dimensions
dim = MrData.Dim;

% Reserve space
img = zeros(dim.nEc, dim.nSl, dim.nRe, dim.nRe, dim.nSe, 'single');
tmp = zeros(dim.nRe, dim.nRe, dim.nCh);

% Dimensions of the resulting image
imDim = [dim.nRe, dim.nRe];

h = waitbar(0, 'Reconstructing data...');
for se=1:dim.nSe
    lines = ~MrData.noiseMask(1,:,se);
    if sum(lines(:)) > 0
    k = reshape(trj.k(1,:,lines,se),1,[]);
    w = reshape(trj.w(1,:,lines,se),[],1);
        % Same NUFFT operator for all echoes, slices, and channels
        FT = NUFFT(k, w, 1, 0, imDim, 2);
        for ec = 1:dim.nEc
            for sl = 1:dim.nSl
                for ch=1:dim.nCh
                    % Apply NUFFT
                    tmp(:,:,ch) = FT'*double(reshape(MrData.data(ec,sl,ch,:,lines,se),[],1));
                end %/channel
                % Combine channels
                img(ec,sl,:,:,se) = sum(tmp.*squeeze(conj(I_rx(sl,:,:,:))),3);
            end %/slice
        end %/echo
    else
        % Do not reconstruct the sets without flip angles
        img(:,:,:,:,se) = 0;
    end
    waitbar(se/dim.nSe)
end %/set
close(h)
end

