%%-----------------------------------------------------------------------%%
%  Authors: M.H.C. van Riel and M.A. Cloos
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 16
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
function show_opt_result(T1, B1, m0, initFAs, optFAs, seqParam)
%Shows the result of the flip angle optimization
%   Inputs:
%   T1: T1 relaxation time in ms
%   B1: relative B1 strength
%   m0: equilibrium magnetization, proportional to proton density
%   initFAs: initial flip angles before optimization in radians
%   optFAs: optimized flip angle pattern in radians
%   seqParam: sequence parameters used for fingerprint simulation

% Show flip angles
figure
subplot(2,2,1)
plot([initFAs*180/pi, optFAs*180/pi], 'LineWidth', 2)
xlabel('Set Index')
ylabel('Flip angle (degrees)')
legend('Initial', 'Optimized')
title('Optimized flip angle train')

% Show signal
spTmp = seqParam;
spTmp.normalize = false;
fpInit = sim_fingerprint(T1, B1, m0, initFAs, spTmp);
fpOpt = sim_fingerprint(T1, B1, m0, optFAs, spTmp);
nFP = length(fpOpt);
subplot(2,2,3)
plot([fpInit, fpOpt], 'LineWidth', 2)
hold on, plot([0 nFP], [0 0], 'k', 'LineWidth', 1)
xlabel('Set Index')
ylabel('Unnormalized signal')
legend('Initial', 'Optimized')
title('Optimized signal')

fpOptNorm = sim_fingerprint(T1, B1, m0, optFAs, seqParam);
fpOptLowT1 = sim_fingerprint(T1*2/3, B1, m0, optFAs, seqParam);
fpOptHighT1 = sim_fingerprint(T1*4/3, B1, m0, optFAs, seqParam);
subplot(2,2,2)
plot([fpOptLowT1, fpOptNorm, fpOptHighT1], 'LineWidth', 2)
hold on, plot([0 nFP], [0 0], 'k', 'LineWidth', 1)
xlabel('Set Index')
ylabel('Fingerprint signal')
legend(sprintf('{\\itT}_1 = %.1f', T1*2/3), sprintf('{\\itT}_1 = %.1f', T1), sprintf('{\\itT}_1 = %.1f', T1/4*3))
title('Optimized T1 dependence')

% Show T1 dependece
fpOptLowB1 = sim_fingerprint(T1, B1*0.75, m0, optFAs, seqParam);
fpOptHighB1 = sim_fingerprint(T1, B1*1.25, m0, optFAs, seqParam);
subplot(2,2,4)
plot([fpOptLowB1, fpOptNorm, fpOptHighB1], 'LineWidth', 2)
hold on, plot([0 nFP], [0 0], 'k', 'LineWidth', 1)
xlabel('Set Index')
ylabel('Fingerprint signal')
legend(sprintf('{\\itB}_1^+ = %.2f', B1*0.75), sprintf('{\\itB}_1^+ = %.2f', B1), ...
    sprintf('{\\itB}_1^+ = %.1f', B1*1.25))
title('Optimized B1 dependence')

end

