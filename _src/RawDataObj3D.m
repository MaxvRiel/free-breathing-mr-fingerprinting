classdef RawDataObj3D < handle

  properties
    data
    noiseCov
    angles
    noiseMask
    ordering
    softwareVersion
    Dim
    
    bNoiseExtracted = false;
    bIsCompressed = false;
    bIsMasked = false;
    bIsDecorr = false;
    bIsCoilCompressed = false;
  end

  methods
    %% ---------------------------------------------------------------------------
    %% Constructor
    %% ---------------------------------------------------------------------------
    function obj = RawDataObj3D(input_file_path, input_file_name)
      
      % Load data .mat file
      load([input_file_path, input_file_name], ...
          'softwareVersion', 'Dim', 'angles', 'noiseMask', 'data');
      obj.softwareVersion = softwareVersion;
      obj.Dim = Dim;
      obj.angles = angles;
      obj.noiseMask = noiseMask;
      obj.data = data;

    end % /constructor
    
    % ---------------------------------------------------------------------------
    % Redistribute sets
    % ---------------------------------------------------------------------------
    function groupSets(obj, nSeqSets)
        
        newNSe = obj.Dim.nSe / nSeqSets;
        newNLi = obj.Dim.nLi * nSeqSets;
        
        if obj.Dim.nSe == newNSe
            return
        end
        
        if mod(obj.Dim.nSe, nSeqSets) ~= 0
            error([num2str(obj.Dim.nSe), ' sets cannot be regrouped into ', ...
                num2str(newNSe), ' sets'])
        end
        
        obj.data = reshape(obj.data, obj.Dim.nEc, obj.Dim.nSl, obj.Dim.nCh, ...
            obj.Dim.nRe, newNLi, newNSe);
        
        obj.angles = reshape(obj.angles, obj.Dim.nEc, newNLi, newNSe);
        
        obj.noiseMask = reshape(obj.noiseMask, obj.Dim.nEc, newNLi, newNSe);
        
        obj.Dim.nLi = newNLi;
        obj.Dim.nSe = newNSe;
    end
    
    % ---------------------------------------------------------------------------
    % set slice range
    % ---------------------------------------------------------------------------
    function setSliceRange(obj, slice_range)
        if ~isequal(slice_range, 0) && ~isequal(slice_range, 1:obj.Dim.nSl)
            obj.data = obj.data(:,slice_range,:,:,:,:);
            obj.Dim.nSl = size(obj.data,2);
        end
    end
    
    % ---------------------------------------------------------------------------
    % set ordering
    % ---------------------------------------------------------------------------
    function setOrdering(obj, ord)
        assert(strcmp(ord, 'mr') || strcmp(ord, 'norm'), ...
            'Invalid ordering given, use ''norm'' or ''mr''')
        obj.ordering = ord;
    end
    
    % ---------------------------------------------------------------------------
    % Extract noise from data without flip angles
    % ---------------------------------------------------------------------------    
    function extractNoise(obj, plotLevel)
        
        % Remove the sets directly following excitations from the noise
        % mask, as these still contain some signal
        if strcmp(obj.ordering, 'mr')
            nm = obj.noiseMask & circshift(obj.noiseMask, 1, 3) & circshift(obj.noiseMask, 2, 3);
            nm(:,:,1:2) = false;
        elseif strcmp(obj.ordering, 'norm')
            nm = obj.noiseMask & circshift(obj.noiseMask, obj.Dim.nSl, 3) & ...
                circshift(obj.noiseMask, 2*obj.Dim.nSl, 3);
            nm(:,:,1:2*obj.Dim.nSl) = false;
        end
        
        nNoisePoints = sum(nm(:));
        if nNoisePoints == 0 || obj.bIsMasked
            warning('No noise data available, using identity matrix for noise covariance')
            obj.noiseCov = eye(obj.Dim.nCh);
            
            obj.bNoiseExtracted = true;
            return
        end
        
        noise = complex(zeros(nNoisePoints*obj.Dim.nSl*obj.Dim.nRe, obj.Dim.nCh, 'single'));
        
        % Extract all noise measurements, for each channel separately
        iStart = 1;
        for ec = 1:obj.Dim.nEc
            for sl = 1:obj.Dim.nSl
                for re = 1:obj.Dim.nRe
                    n = obj.data(ec,sl,:,re,nm(ec,:));
                    n = permute(n, [1,2,4,5,3]);
                    n = reshape(n, [], obj.Dim.nCh);
                    iEnd = iStart + size(n,1) - 1;
                    noise(iStart:iEnd,:) = n;
                    iStart = iEnd + 1;
                end
            end
        end
        
        % Calculate noise covariance matrix
        obj.noiseCov = cov(noise);
        
        obj.bNoiseExtracted = true;
        
        if plotLevel >= 2
            figure
            subplot(1,2,1)
            nPlotNoise = min(size(noise, 1), 180);
            imshow(abs(noise(1:nPlotNoise, :)), [])
            title(['Noise data, ', num2str(nNoisePoints*obj.Dim.nSl*obj.Dim.nRe), ...
                ' data points in total'])
            
            subplot(1,2,2)
            imshow(abs(obj.noiseCov), [])
            title('Noise covariance matrix')
        end
        
    end
    
    % ---------------------------------------------------------------------------
    % Zero out all sets without flip angles
    % ---------------------------------------------------------------------------
    function maskData(obj)
        
        for ec = 1:obj.Dim.nEc
            obj.data(ec,:,:,:,obj.noiseMask(ec,:)) = 0;
        end
        
        obj.bIsMasked = true;
    end
    
    % ---------------------------------------------------------------------------
    % Decorrelate the channels using the noise covariance matrix
    % ---------------------------------------------------------------------------
    function decorrChannels(obj)
        
        if isequal(obj.noiseCov, eye(obj.Dim.nCh))
            disp('Skipping decorrelation')
            return
        end
        
        if ~obj.bNoiseExtracted
            error('Extract noise first to obtain noise covariance matrix')
        end
        
        psi = inv(obj.noiseCov)^(1/2);
        for ec = 1:obj.Dim.nEc
            if obj.bIsMasked
                lines = ~obj.noiseMask(ec,:);
            else
                lines = true(ec,obj.Dim.nLi*obj.Dim.nSe);
            end
            for sl = 1:obj.Dim.nSl
                Xsl = reshape(obj.data(ec,sl,:,:,lines), obj.Dim.nCh, []);
                XslDecorr = psi * Xsl;
                obj.data(ec,sl,:,:,lines) = reshape(XslDecorr, obj.Dim.nCh, obj.Dim.nRe, []);
            end
        end
        
        obj.noiseCov = eye(obj.Dim.nCh); 
        
        obj.bIsDecorr = true;
        
    end
    
    % ---------------------------------------------------------------------------
    % Apply Fourier transform in the slice direction
    % ---------------------------------------------------------------------------

    function sliceFT(obj)
        dRe = 30;
        fft_shift = ceil(obj.Dim.nSl/2);
        ifft_shift = floor(obj.Dim.nSl/2);
        for iStart = 1:dRe:obj.Dim.nRe
            iEnd = min(iStart+dRe-1, obj.Dim.nRe);
            obj.data(:,:,:,iStart:iEnd,:,:) = obj.data(:,[fft_shift+1:end, 1:fft_shift],:,iStart:iEnd,:,:);
            obj.data(:,:,:,iStart:iEnd,:,:) = ifft(obj.data(:,:,:,iStart:iEnd,:,:),[],2);
            obj.data(:,:,:,iStart:iEnd,:,:) = obj.data(:,[ifft_shift+1:end, 1:ifft_shift],:,iStart:iEnd,:,:);
        end
    end
    
    % ---------------------------------------------------------------------------
    % Apply coil compression
    % ---------------------------------------------------------------------------
    function energy = coilCompression(obj, nVirtCoils, CSPart, plotLevel)
        
        if nVirtCoils == 0
            return
        end
        
        if ~obj.bIsDecorr
            warning('Applying coil compression without decorrelating channels')
            obj.noiseCov = [];
        else
            obj.noiseCov = eye(nVirtCoils);
        end
        
        if nVirtCoils > obj.Dim.nCh
            warning(['Too many virtual coils given, using ', num2str(obj.Dim.nCh), ' virtual coils instead'])
            nVirtCoils = obj.Dim.nCh;
        end
        
        energy = zeros(obj.Dim.nEc, obj.Dim.nSl);
        
        % Only select the center of each spoke to calculate the compression
        % matrix
        reFilter = zeros(1, obj.Dim.nRe);
        reFilter(round((1-CSPart)/2*obj.Dim.nRe+1):round((1+CSPart)/2*obj.Dim.nRe)) = hann(CSPart*obj.Dim.nRe);
        
        for ec = 1:obj.Dim.nEc
            if obj.bIsMasked
                lines = ~obj.noiseMask(ec,:);
            else
                lines = true(1,obj.Dim.nLi*obj.Dim.nSe);
            end

            % Perform SVD on coils for each slice
            for sl = 1:obj.Dim.nSl
                % Construct data array
                Xsl = obj.data(ec,sl,:,:,lines);
                Xsl = reshape(Xsl, obj.Dim.nCh, []);
                XslCenter = Xsl .* repmat(reFilter, 1, sum(lines));
                % Perform SVD compression
                [Ux,Sx,~] = svd(XslCenter, 'econ');
                % Select first N principal components
                Ux = Ux(:, 1:nVirtCoils);
                Sx = diag(Sx.^2);
                % Compress data
                XslCompressed = Ux' * Xsl;
                % Fill in data array
                XslCompressed = reshape(XslCompressed, 1, 1, nVirtCoils, obj.Dim.nRe, []);
                obj.data(ec,sl,1:nVirtCoils,:,lines) = XslCompressed;
                
                % Calculate retained energy
                e = sum(Sx(1:nVirtCoils))/sum(Sx);
                
                energy(ec, sl) = e;
                
                % Show energy distribution of singular vectors
                if plotLevel >= 2 && sl == round(obj.Dim.nSl/2)
                    figure('DefaultAxesFontSize', 12)
                    hold on
                    plot(cumsum(Sx)/sum(Sx), 'LineWidth', 1.5)
                    yl = ylim;
                    ylim([-Inf, 1])
                    plot([1, length(Sx)], [e, e], 'k--')
                    plot([nVirtCoils, nVirtCoils], [yl(1), 1], 'k--')
                    hold off
                    xlabel('Singular values')
                    ylabel('Relative energy')
                    title('Energy distribution of SVs for coil compression')
                end
            end
        end
        
        obj.data = obj.data(:,:,1:nVirtCoils,:,:,:);
        
        obj.Dim.nCh = nVirtCoils;
        
        obj.bIsCoilCompressed = true;
        
    end

    % ---------------------------------------------------------------------------
    % Get k space trajectory
    % ---------------------------------------------------------------------------
    function trj = get_trajectory(obj)

      gbc.x = zeros(obj.Dim.nEc, obj.Dim.nRe, obj.Dim.nLi, obj.Dim.nSe);
      gbc.y = zeros(obj.Dim.nEc, obj.Dim.nRe, obj.Dim.nLi, obj.Dim.nSe);

      gbc.rho = 1:obj.Dim.nRe;
      gbc.rho = gbc.rho - (obj.Dim.nRe+1)/2;
      gbc.rho = gbc.rho/obj.Dim.nRe;

      for ec=1:obj.Dim.nEc
        for re=1:obj.Dim.nRe
          for li=1:obj.Dim.nLi
            for se=1:obj.Dim.nSe
                gbc.x(ec,re,li,se)=gbc.rho(re)*sin(obj.angles(ec, li, se));
                gbc.y(ec,re,li,se)=gbc.rho(re)*cos(obj.angles(ec, li, se));
            end
          end
        end
      end

      trj.k = gbc.x+1i*gbc.y;
      trj.w = sqrt(gbc.x.^2+gbc.y.^2);
      trj.w = trj.w/(max(trj.w(:)));

      clear gbc; % GarBage Collector
    end

    % ---------------------------------------------------------------------------
    % Apply an arbitrary compression
    % ---------------------------------------------------------------------------
    function applyCompression(obj, U)
    
    if isempty(U)
        warning('Dictionary not compressed')
        return
    end

    dataT = complex(zeros(obj.Dim.nEc, obj.Dim.nSl, obj.Dim.nCh,...
        obj.Dim.nRe, obj.Dim.nLi*obj.Dim.nSe, size(U,2), 'single'));

    for i=1:size(U,2)
        u = squeeze(permute(repmat(U(:,i),[1,obj.Dim.nRe,obj.Dim.nLi]),[2,3,1])); % readout spokes k-data
        for ec=1:obj.Dim.nEc
            for sl=1:obj.Dim.nSl
                for ch=1:obj.Dim.nCh
                    tmp = squeeze(obj.data(ec,sl,ch,:,:,:)).*u;
                    dataT(ec,sl,ch,:,:,i) = tmp(:,:);
                end
            end
        end
    end

    obj.data = dataT;
    clear('dataT');

    obj.angles = obj.angles(:, :);
    obj.angles = repmat(obj.angles,1,1,size(U,2));
    
    obj.noiseMask = obj.noiseMask(:,:);
    obj.noiseMask = repmat(obj.noiseMask,1,1,size(U,2));

    obj.Dim.nLi = obj.Dim.nLi*obj.Dim.nSe;
    obj.Dim.nSe = size(U,2);

    obj.bIsCompressed = true;

    end
    
  end % /methods

end % /class
