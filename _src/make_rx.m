%%-----------------------------------------------------------------------%%
%  Authors: M.A. Cloos
%  Martijn.Cloos@nyumc.org
%  Date: 2019 March 3
%  New York University School of Medicine, www.cai2r.net
%
%  Modified: Max van Riel
%   - Exclude the lines without flip angles
%   - Added filter to create smooth sensitivity maps
%%-----------------------------------------------------------------------%%
function [I_rx] = make_rx(MrData, CSPart)

%%-----------------------------------------------------------------------%%
% Extract the trajectory & dimensions
%%-----------------------------------------------------------------------%%
    trj = get_trajectory(MrData);
    dim = MrData.Dim;    
%%-----------------------------------------------------------------------%%
% Reconstruct the images
%%-----------------------------------------------------------------------%%
    I_rx = zeros(dim.nSl,dim.nRe,dim.nRe,dim.nCh, 'single');
    echo = 1;
    
    lines = ~MrData.noiseMask(echo,:);
    reFilter = zeros(dim.nRe,1);
    reFilter(round((1-CSPart)/2*dim.nRe+1):round((1+CSPart)/2*dim.nRe)) = hann(CSPart*dim.nRe);
    FT = NUFFT(squeeze(trj.k(1,:,lines)), squeeze(trj.w(1,:,lines)), 1, 0, [dim.nRe,dim.nRe], 2);
    for sl=1:dim.nSl
        for ch=1:dim.nCh
            I_rx(sl,:,:,ch)=FT'*(double(squeeze(MrData.data(echo,sl,ch,:,lines))) .* reFilter);
        end
    end

%%-----------------------------------------------------------------------%%
% Extract images from radial pre-scan
%%-----------------------------------------------------------------------%%
    I_body__rx = sqrt(abs(sum(I_rx.*conj(I_rx),4)));
    
%%-----------------------------------------------------------------------%%
% Calculate ratios
%%-----------------------------------------------------------------------%%
    I_rx = I_rx./repmat(I_body__rx,1,1,1,size(I_rx,4));
    
%%-----------------------------------------------------------------------%%
% Normalize
%%-----------------------------------------------------------------------%%     
    I_rx = I_rx/max(abs(I_rx(:)));

end