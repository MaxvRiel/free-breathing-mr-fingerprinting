%%-----------------------------------------------------------------------%%
%  Authors: M.A. Cloos
%  Martijn.Cloos@nyumc.org
%  Date: 2019 april 23
%  New York University School of Medicine, www.cai2r.net
%
%  Modified: Max van Riel
%   - Included mask to increase speed
%%-----------------------------------------------------------------------%%
function [ maps, fits ] = fast_match( img , dictionary, partSize, apply_mask, mask )

[nSl, nRe, nPh, ~] = size(img);
[nSv, nDi]         = size(dictionary.atoms);

if ~apply_mask
    mask = true(nSl, nRe, nPh);
end

nMatchVox      = nnz(mask);

posm  = zeros(ceil(nDi/partSize), nMatchVox);
corm  = complex(zeros(ceil(nDi/partSize), nMatchVox));
index = 1;


imv = reshape(img, [ nSl*nRe*nPh nSv ]);
imv = imv(mask(:),:);
imv = single(imv');

%%-----------------------------------------------------------------------%%
% Matching
%%-----------------------------------------------------------------------%%
pb2 = CmdLineProgressBar(' Matching: '); 
for ds=1:partSize:nDi
    de = min(ds + partSize - 1, nDi);
   
    atom = squeeze(dictionary.atoms(:,(ds:de)));
    atom = atom';
    [cor, pos] = max(atom*imv);
    posm(index, :)  = pos(:) + ds - 1;
    corm(index, :)  = cor(:);
    index = index + 1;
    pb2.print(de,nDi);
end

%%-----------------------------------------------------------------------%%
% Pick est matches from look uptable
%%-----------------------------------------------------------------------%%
[pdFact, pos] = max(corm, [], 1);

mapsTmp  = complex(zeros(5  ,nMatchVox));
fitsTmp  = complex(zeros(nSv,nMatchVox));
for i=1:nMatchVox
    mapsTmp(:,i) = dictionary.lookup(:,posm(pos(i),i));
    fitsTmp(:,i) = dictionary.atoms(:,posm(pos(i),i));
end

maps  = complex(zeros(5  ,nSl*nRe*nPh));
fits  = complex(zeros(nSv,nSl*nRe*nPh));
maps(:, mask(:)) = mapsTmp;
fits(:, mask(:)) = fitsTmp;
clear mapsTmp fitsTmp

%%-----------------------------------------------------------------------%%
% Convert norm to PD
%%-----------------------------------------------------------------------%%
maps(1,mask(:)) =  pdFact./maps(1,mask(:));

%%-----------------------------------------------------------------------%%
% rescale svd images with PD
%%-----------------------------------------------------------------------%%
pdFact = repmat(pdFact,[nSv 1]);
fits(:,mask(:))   = fits(:,mask(:)).*pdFact;

%%-----------------------------------------------------------------------%%
% reshape
%%-----------------------------------------------------------------------%%
maps = reshape(maps, [5, nSl,nRe,nPh]);
fits = reshape(fits, [nSv, nSl,nRe,nPh]);

% normalize the PD in the images
maps(1,:,:,:) = squeeze(maps(1,:,:,:)./(max(abs(squeeze(maps(1,:))))));

end