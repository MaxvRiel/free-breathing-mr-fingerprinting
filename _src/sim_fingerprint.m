%%-----------------------------------------------------------------------%%
%  Authors: M.H.C. van Riel and M.A. Cloos
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 16
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
function [fp, fpNorm] = sim_fingerprint(T1, B1, m0, FAs, seqParam)
%Simulates the signal evolution (fingerprint) of one spin given a train of 
%flip angles using a FLASH sequence. Assumes instantaneous pulses and 
%perfect spoiling.
%   Inputs:
%   T1: 1 x N vector of T1 relaxation times in ms
%   B1: 1 x N vector of relative B1 strengths
%   m0: equilibrium magnetization, proportional to proton density
%   FAs: vector of flip angles in radians
%   seqParam: parameters of the sequence, including TR, inversion, 
%       inversion time, delay time, number of shots, number of partitions,
%       and normalization
%
%   Output:
%   fp: fingerprint (transverse magnetization after each flip angle, 
%       optionally normalized and averaged)
%   fpNorm: norm of the fingerprint before normalization

% Without arguments, test the function
if nargin < 1
    test_sim_fingerprint()
    return
end

% Extract parameters
TR = seqParam.TR;
inversion = seqParam.inversion;
tInv = seqParam.tInv;
tDelay = seqParam.tDelay;
nShots = seqParam.nShots;
nPartitions = seqParam.nPartitions;
normalize = seqParam.normalize;

% Check dimensions
FAs = FAs(:);
T1 = T1(:)';
B1 = B1(:)';
nFAs = length(FAs);
nFPs = length(T1);
assert(nFPs == length(B1), 'T1 and B1 vectors must have the same length')

% Scale flip angles with B1 strength
FAs_B1 = repmat(FAs, 1, nFPs) .* repmat(B1, nFAs, 1);

% Pre-compute flip angle rotations
cosFAs = cos(FAs_B1);
sinFAs = sin(FAs_B1);

% Pre-compute exponentials
ETR = exp(-TR./T1);
m0_ETR = m0*(1-ETR);
Einv = exp(-tInv./T1);
m0_Einv = m0*(1-Einv);
Edelay = exp(-(tDelay+5)./T1);      % Always include 5 ms for inversion pulse
m0_Edelay = m0*(1-Edelay);

cosFAs_ETR = cosFAs .* repmat(ETR, nFAs, 1);

% Initialize longitudinal magnetization
mZ = m0 * ones(1, nFPs);

% Create fingerprint array
fpFull = zeros(nFAs, nFPs, 'like', cosFAs_ETR);

for iShot = 1:nShots
    % Assume perfect inversion
    if inversion
        mZ = -mZ.*Einv + m0_Einv;
    end
    
    % Excitations. Assume perfect spoiling.
    for iFA = 1:nFAs
        if iShot == nShots
            mT = mZ .* sinFAs(iFA, :);
            fpFull(iFA,:) = mT;
        end
        mZ = mZ .* cosFAs_ETR(iFA,:) + m0_ETR;
    end
    
    % Delay
    mZ = mZ .* Edelay + m0_Edelay;
end

if seqParam.averageSets
    fpAvg = zeros(nFAs/nPartitions, nFPs, 'like', fpFull);
    for iFP = 1:nFPs
        fpAvg(:,iFP) = mean(reshape(fpFull(:,iFP), nPartitions, nFAs/nPartitions));
    end
else
    fpAvg = fpFull;
end

if normalize
    fpNorm = sqrt(sum(fpAvg.^2));
    fp = fpAvg ./ repmat(fpNorm, size(fpAvg,1), 1);
else
    fpNorm = ones(1, nFPs);
    fp = fpAvg;
end

end

function test_sim_fingerprint()

m0 = 1;
% Sequence parameters
seqParam.TR = 5;
seqParam.inversion = true;
seqParam.tInv = 2*seqParam.TR;
seqParam.tDelay = 0;
seqParam.nShots = 2;
seqParam.nPartitions = 30;
seqParam.averageSets = false;
seqParam.normalize = true;

T1 = [750, 500, 1000, 750, 750];
B1 = [1, 1, 1, 0.75, 1.25];
% T1 = 750;
% B1 = 1.1;
nFPs = length(T1);

load('../flip_angles/opt_FAs-600.mat', 'FAs')
FAs = FAs * pi / 180;
nFAs = length(FAs);

% Calculate fingerprints and time function calls
% Repeat timing multiple times
repeats = 1000;

% Use SX casadi function
T1_SX = casadi.SX.sym('T1', 1, nFPs);
B1_SX = casadi.SX.sym('B1', 1, nFPs);
FAs_SX = casadi.SX.sym('FAs', nFAs);

tic;
fp = sim_fingerprint(T1_SX, B1_SX, m0, FAs_SX, seqParam);
fpFcn = casadi.Function('fp_function', {T1_SX, B1_SX, FAs_SX}, {fp}, ...
    {'T1', 'B1', 'Flip angles'}, {'fingerprint'});
fpFullFcn = returntypes('full', fpFcn);
t_end = toc;
fprintf('Casadi SX preparation: %.3e s\n',t_end)
fprintf('Casadi SX nodes: %d\n', fpFcn.n_nodes())

tic;
for i = 1:repeats
    fp_SX = fpFullFcn(T1, B1, FAs);
end
t_end = toc;
fprintf('Casadi SX call: %.3e s\n', t_end/repeats)

% Use MX casadi function
T1_MX = casadi.MX.sym('T1', 1, nFPs);
B1_MX = casadi.MX.sym('B1', 1, nFPs);
FAs_MX = casadi.MX.sym('FAs', nFAs);

tic;
fp = sim_fingerprint(T1_MX, B1_MX, m0, FAs_MX, seqParam);
fpFcn = casadi.Function('fp_function', {T1_MX, B1_MX, FAs_MX}, {fp}, ...
    {'T1', 'B1', 'Flip angles'}, {'fingerprint'});
fpFullFcn = returntypes('full', fpFcn);
t_end = toc;
fprintf('Casadi MX preparation: %.3e s\n', t_end)
fprintf('Casadi MX nodes: %d\n', fpFcn.n_nodes())

tic;
for i = 1:repeats
    fp_MX = fpFullFcn(T1, B1, FAs);
end
t_end = toc;
fprintf('Casadi MX call: %.3e s\n', t_end/repeats)

% Use normal Matlab classes
tic
for i = 1:repeats
    fp = sim_fingerprint(T1, B1, m0, FAs, seqParam);
end
t_end = toc;
fprintf('Numerical call: %.3e s\n', t_end/repeats)

% Plot signal
figure
hold on
plot(fp_SX, 'LineWidth', 2)
plot(fp_MX, 'LineWidth', 2)
plot(fp, 'LineWidth', 2)
xlabel('Set Index')
ylabel('Signal amplitude')
title('Fingerprint signals')
legend('Casadi SX', 'Casadi MX', 'Matlab')
set(gca, 'FontSize', 12)
hold off

% Plot signal difference, should be very small
figure
hold on
plot(fp_SX - fp, 'LineWidth', 2)
plot(fp_MX - fp, 'LineWidth', 2)
xlabel('Set Index')
ylabel('Signal amplitude')
title('Differences between the simulation methods')
legend('Casadi SX', 'Casadi MX')
set(gca, 'FontSize', 12)
hold off

end

