%%-----------------------------------------------------------------------%%
%  Authors: M.H.C. van Riel and M.A. Cloos
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 14
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
function show_maps(maps, sliceIdx, ranges)
%Shows the result of the dictionary matching.
%   Inputs:
%   maps: dim.nEc x 4 x dim.nSl x dim.nRe x dim.nRe array containing the
%       quantitative maps. The four dimensions are: 
%       1. PD magnitude
%       2. PD phase
%       3. T1
%       4. Relative B1+
%   sliceIdx: slice index used for visualization
%   ranges: 1 x 3 cell array with the parameter ranges used to determine
%       the color limits in the plots

figure('DefaultAxesFontSize', 12)
subplot(1,3,1); imshow(squeeze(maps(1,1,sliceIdx,:,:)),     ranges{1}); title('PD');
subplot(1,3,2); imshow(squeeze(maps(1,3,sliceIdx,:,:)),     ranges{2}); title('T1');  
subplot(1,3,3); imshow(squeeze(maps(1,4,sliceIdx,:,:)),     ranges{3}); title('B1'); 

for i = 1:3
    pos = get(subplot(1,3,i), 'Position');
    colorbar('Position', [pos(1)+pos(3)+0.01, 0.3, 0.02, 0.436])
end

end

