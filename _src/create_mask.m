%%-----------------------------------------------------------------------%%
%  Authors: M.H.C. van Riel and M.A. Cloos
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 14
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
function mask = create_mask(img, maskMethod, slice, plotLevel)
%Create a mask, either by drawing a manual ROI or by thresholding the image
%   Inputs:
%   img: dim.nEc x dim.nSl x dim.nRe x dim.nRe x dim.nSe array of the 
%       reconstructed images
%   maskMethod: either 'circles' or 'threshold'
%   slice: slice index used for visualization
%   plotLevel: plot resulting mask only if plotLevel >= 2
%
%   Outputs:
%   mask: dim.nSl x dim.nRe x dim.nRe mask of the interesting region(s) of 
%       the images

mask = false(size(img,2), size(img,3), size(img,4));
% Sum of squares of all sets
sumImg = reshape(sum(abs(img).^2, [1,5]), size(mask));
% Normalize
sumImg = sumImg ./ prctile(sumImg(:), 95);

if strcmp(maskMethod, 'circles')
    
    [X, Y] = meshgrid(1:size(img,3), 1:size(img,4));
    
    for sl = 1:size(img, 2)
        % Detect the most circular object
        slImg = squeeze(sumImg(sl,:,:));
        [centers, radii, score] = imfindcircles(slImg, 53, 'Sensitivity', 1, 'Method', 'TwoStage', 'EdgeThreshold', 0);
        if ~isempty(score) && score(1) > 0
            mask(sl,:,:) = (X - centers(1,1)).^2 + (Y - centers(1,2)).^2 <= radii(1)^2;
        end
    end
    
elseif strcmp(maskMethod, 'threshold')
    % Threshold image
    T = adaptthresh(sumImg, 0.8);
    mask = imbinarize(sumImg, T);
    % Fill in holes and select central part of the mask for each slice
    for iSl = 1:size(mask, 1)
        mask(iSl,:,:) = imfill(squeeze(mask(iSl,:,:)), 'holes');
        mask(iSl,:,:) = bwselect(squeeze(mask(iSl,:,:)), floor(size(mask,2))/2+1, floor(size(mask,3))/2+1);
    end
    
else
    error('Unknown masking method; must be either ''circles'' or ''threshold''')
end

if plotLevel >= 2
    figure
    subplot(1,2,1)
    imshow(flip(squeeze(sumImg(slice,:,:))), [0, prctile(sumImg(:), 98)]);
    title('Sum of squares')
    subplot(1,2,2)
    imshow(flip(squeeze(mask(slice,:,:))), [])
    title('Mask')
end

end

