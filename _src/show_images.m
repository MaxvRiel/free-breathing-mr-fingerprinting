%%-----------------------------------------------------------------------%%
%  Authors: M.H.C. van Riel and M.A. Cloos
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 14
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
function show_images(img, mask, setIdx, slice)
%Show the images for a selected number of sets
%   Inputs:
%   img: dim.nEc x dim.nSl x dim.nRe x dim.nRe x dim.nSe array of the 
%       reconstructed images
%   mask: dim.nSl x dim.nRe x dim.nRe logical array with the mask
%   setIdx: indices of the sets used for visualization
%   slice: index of the slice used for visualization

figure; 
for iSe = 1:length(setIdx)
    se = setIdx(iSe);
    subplot(2,ceil(length(setIdx)/2),iSe)
    imshow(abs(squeeze(img(1,slice,:,:,se))) .* squeeze(mask(slice,:,:)),[]);
    title(['\bf Set #', num2str(se)])
end

end