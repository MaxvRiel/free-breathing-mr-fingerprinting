%%-----------------------------------------------------------------------%%
%  Authors: M.H.C. van Riel and M.A. Cloos
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 14
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
function visualize_rx(I_rx, slice, nRows)
%Function to visualize the receive profiles
%   Inputs:
%   I_rx: 4-D array with dimensions: [slices, x, y, channels]
%   slice: which slice to show
%   nRows: number of rows for the subplots (optional, default = 2)

% Create subplot layout
if nargin < 3 || isempty(nRows)
    nRows = 2;
end
nCols = ceil(size(I_rx, 4)/nRows);

figure

lim = [min(abs(I_rx(:))), max(abs(I_rx(:)))];

for i = 1:size(I_rx, 4)

    subplot(nRows, nCols, i)
    imshow(abs(squeeze(I_rx(slice, :, :, i))), lim)
    if i == ceil(nCols/2)
        title('Magnitude of receive profiles')
    end
    
end

figure

for i = 1:size(I_rx, 4)
    
    subplot(nRows, nCols, i)
    imshow(angle(squeeze(I_rx(slice, :, :, i))), [-pi pi])
    if i == ceil(nCols/2)
        title('Phase of receive profiles')
    end
        
end

