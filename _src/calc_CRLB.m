%%-----------------------------------------------------------------------%%
%  Authors: M.H.C. van Riel and M.A. Cloos
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 16
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
function [CRLB, jac] = calc_CRLB(T1, B1, m0, FAs, seqParam, jacAutodiff)
%Calculate the Cram�r-Rao lower bound.
%   Inputs:
%   T1: T1 relaxation time in ms
%   B1: relative B1 strength
%   m0: equilibrium magnetization, proportional to proton density
%   FAs: vector of flip angles in radians
%   seqParam: sequence parameters used for fingerprint simulation
%   jacAutodiff: use automatic differentiation for the Jacobian or not
%
%   Outputs:
%   CRLB: Cram�r-Rao Lower Bound
%   jac: Jacobian matrix of the fingerprint wrt T1 and B1+

% Without arguments, test the function
if nargin < 1
    test_calc_CRLB()
    return
end

% Set up weighting matrix
if seqParam.normalize
    W = diag([1/T1^2, 1/B1^2]);
else
    W = diag([0, 1/T1^2, 1/B1^2]);
end

if jacAutodiff && check_casadi_types(T1) && check_casadi_types(B1)
    % Automatic differentiation
    [fp, fpNorm] = sim_fingerprint(T1, B1, m0, FAs, seqParam);
    jac = jacobian(fp, [T1; B1]);
else
    % Numerical differentiation    
    stepSize = 1e-7;
    [fp, fpNorm] = sim_fingerprint(T1, B1, m0, FAs, seqParam);
    fp2 = sim_fingerprint(T1*(1+stepSize), B1, m0, FAs, seqParam);
    fp3 = sim_fingerprint(T1, B1*(1+stepSize), m0, FAs, seqParam);

    dm_dT1 = (fp2-fp)/(stepSize*T1);
    dm_dB1 = (fp3-fp)/(stepSize*B1);

    jac = [dm_dT1, dm_dB1];
end

if ~seqParam.normalize
    jac = [fp, jac];
end

% Construct Fisher Information Matrix
FIM = jac' * jac;

% Scale the noise in the FIM, normalize by the sequence length, and
% multiply by a factor to prevent small values
FIM = FIM * 1e+7 * (fpNorm/seqParam.seqLength)^2;

% Invert FIM and apply weighting matrix
CRLB = W / FIM;

end

function test_calc_CRLB()

% Use default parameters
m0 = 1;
T1 = 750;
B1 = 1;

load('../flip_angles/opt_FAs-600.mat', 'FAs')
FAs = FAs * pi / 180;

seqParam.TR = 5;
seqParam.inversion = true;
seqParam.tInv = 2*seqParam.TR;
seqParam.tDelay = 0;
seqParam.nShots = 2;
seqParam.nPartitions = 30;
seqParam.seqLength = length(FAs);
seqParam.normalize = false;
seqParam.averageSets = false;

% Create Jacobian weighting matrix
if seqParam.normalize
    jacW = diag([T1, B1]);
else
    jacW = diag([m0, T1, B1]);
end

% Use casadi
repeat = 1000;
T1_SX = casadi.SX.sym('T1');
B1_SX = casadi.SX.sym('B1');
[crlb, jac] = calc_CRLB(T1_SX, B1_SX, m0, FAs, seqParam, true);
crlbFcn = casadi.Function('crlb_function', {T1_SX, B1_SX}, {crlb, jac}, ...
    {'T1', 'B1'}, {'CRLB', 'Jacobian'});
tic
for i = 1:repeat
    [CRLB_ad, jac_ad] = crlbFcn(T1, B1);
    CRLB_ad = full(CRLB_ad);
    jac_ad = full(jac_ad);
end
t_end = toc;
fprintf('Casadi Jacobian: %.3e s\tObjective value: %.3f\n', t_end/repeat, trace(CRLB_ad))
fprintf('Jacobian nodes: %d\n', crlbFcn.n_nodes())

% Use numerical differentiation
repeat = 1000;
tic
for i = 1:repeat
    [CRLB_num, jac_num] = calc_CRLB(T1, B1, m0, FAs, seqParam, false);
end
t_end = toc;
fprintf('Numerical Jacobian: %.3e s\tObjective value: %.3f\n', t_end/repeat, trace(CRLB_num))

% Plot Jacobian and difference
if seqParam.normalize
    leg = {'AD wrt T1', 'AD wrt B1', 'Numerical wrt T1', 'Numerical wrt B1'};
else
    leg = {'AD wrt m0', 'AD wrt T1', 'AD wrt B1', 'Numerical wrt m0', 'Numerical wrt T1', 'Numerical wrt B1'};
end
figure
hold on
plot(jac_ad * jacW, 'LineWidth', 2)
plot(jac_num * jacW, 'LineWidth', 2)
xlabel('Set Index')
ylabel('Relative derivative')
title('Jacobian of the fingerprint')
legend(leg)
hold off

if seqParam.normalize
    leg = {'wrt T1', 'wrt B1'};
else
    leg = {'wrt m0', 'wrt T1', 'wrt B1'};
end
figure
plot((jac_num-jac_ad) * jacW, 'LineWidth', 2)
xlabel('Excitation number')
ylabel('Derivative difference')
title('Difference between the Jacobian calculations')
legend(leg)

end

function b = check_casadi_types(a)

b = (isa(a, 'casadi.SX') || isa(a, 'casadi.MX'));

end