%%-----------------------------------------------------------------------%%
%  Authors: M.H.C. van Riel and M.A. Cloos
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 14
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
function dictionary = create_dictionary(T1Values, B1Values, m0, seqParam, FAs, plotLevel)
%Create a dictionary containing the fingerprints for different combinations
%of T1 and B1
%   Inputs:
%   T1Values: vector of T1 values (in ms) to include in dictionary
%   B1Values: vector of relative B1 values to include in dictionary
%   m0: equilibrium magnetization, proportional to proton density
%   seqParam: sequence parameters used for fingerprint simulation
%   FAs: vector containing the flip angles in radians
%   plotLevel: plot energy distribution of the SVD compression only if 
%       plotLevel >= 2
%
%   Outputs:
%   dictionary: structure, containing the dictionary fingerprints (atoms)
%       and a lookup table with the corresponding T1 and B1 values

% Create dictionary structure
dictionary = struct();
nB1 = length(B1Values);
nT1 = length(T1Values);
nDict = nB1*nT1;
dictionary.header.dim = [1,1,nT1,1,nB1];
dictionary.header.TR = seqParam.TR;
dictionary.header.TDelay = seqParam.tDelay;
dictionary.flipAngles = FAs;

fpLength = seqParam.seqLength;
if seqParam.averageSets
    fpLength = fpLength / seqParam.nPartitions;
end
dictionary.lookup = zeros(5, nDict);
dictionary.atoms = zeros(fpLength, nDict);

%% Simulate fingerprints
% Create parameter combinations
T1s = repelem(T1Values, 1, nB1);
B1s = repmat(B1Values, 1, nT1);
[dictionary.atoms, fpNorm] = sim_fingerprint(T1s, B1s, m0, FAs, seqParam);
dictionary.lookup = [fpNorm./m0; ones(1, nDict); T1s; ones(1, nDict); B1s];

%% Apply compression
if seqParam.svdRange > 0
    % Do SVD decomposition
    [U,S,~] = svd(dictionary.atoms,'econ');
    U = U(:,1:seqParam.svdRange);
    
    % Apply compression
    dictionary.atoms = U.' * dictionary.atoms;
    
    % Save U matrix
    dictionary.U = U;
    
    % Plot energy distribution
    if plotLevel >= 2
        Sx = diag(S).^2;
        figure('DefaultAxesFontSize', 12)
        hold on
        plot(cumsum(Sx)/sum(Sx), 'LineWidth', 1.5)
        yl = ylim;
        ylim([-Inf, 1])
        xlim([0, 20])
        energy = sum(Sx(1:seqParam.svdRange))/sum(Sx);
        plot([0, 20], [energy, energy], 'k--')
        plot([seqParam.svdRange, seqParam.svdRange], [yl(1), 1], 'k--')
        hold off
        xlabel('Singular values')
        ylabel('Relative energy')
        title('Energy distribution of SVs for dictionary compression')
    end
    
else
    dictionary.U = [];
end

end