# Free-Breathing MR Fingerprinting #

This repository contains the code that was used to produce the results as presented in "Optimization of MR Fingerprinting for Free-Breathing Quantitative Abdominal Imaging".

It contains two MATLAB scripts:

* recon.m: Use this script to reconstruct the quantitative T1, B1+ and PD maps from the raw data. The data files can be found at Mendeley Data (https://data.mendeley.com/datasets/txpwybnt5p/).

* optimize_flip_angles.m: Use this script to generate optimized flip angle patterns.

The reconstruction script uses the NUFFT implementation from Jeffrey A. Fessler, see: Fessler JA, Sutton BP. Nonuniform Fast Fourier Transforms Using Min-Max Interpolation.

To run the scripts, you need the following MATLAB Toolboxes:

* Image Processing Toolbox (both scripts)

* Optimization Toolbox (optimize_flip_angles.m)

* Signal Processing Toolbox (recon.m)

* Statistics and Machine Learning Toolbox (recon.m)

Furthermore, you need to install the CasADi toolbox from https://web.casadi.org/. 
Make sure that you include the path to the CasADi folder in your MATLAB search path.
