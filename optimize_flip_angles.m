%%-----------------------------------------------------------------------%%
%  Authors: M.A. Cloos and M.H.C. van Riel
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 January 31
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%

% Optimize the flip angle sequence for a motion robust MR Fingerprinting
% experiment using the Cram�r-Rao Lower Bound (CRLB)

% Make sure you have installed the CasADI toolbox and put its folder on
% your path!
% addpath('Path/to/CasADI/folder')

close all; clearvars; clc

addpath(genpath('./_src'));

nFAs = 600;                 % Number of flip angles
nInit = 10;                 % Number of initializations
minFA = 0;                  % Lower bound for the flip angles in radians
maxFA = 60*pi/180;          % Upper bound for the flip angles in radians              
zeroCutoff = 1.5*pi/180;    % Any values below this are set to 0

saveResults = true;

%% Set parameters
T1List = 500:250:1500;      % T1 values [ms]
B1List = [0.75, 1, 1.25];   % Relative B1+ values [-]
m0 = 1;                     % Equilibrium magnetization (proportional to proton density)

% Initialize with every flip angle a random value between 0 and 10 degrees.
% Use a few random points, then interpolate between them using spline
% interpolation.
dFA = 30;
nRand = ceil(nFAs/dFA)+1;
initFAs = rand(nRand, nInit) * 10;
initFAs = interp1(1:dFA:nRand*dFA, initFAs, (1:nFAs)', 'spline');
initFAs = min(max(initFAs, 0), 10);
initFAs = initFAs * pi / 180;       % Convert to radians

% Fingerprint settings
seqParam.TR = 5;                % Repetition time [ms]
seqParam.inversion = true;      % Use inversion pulse or not
seqParam.tInv = 2*seqParam.TR;  % Inversion time [ms]
seqParam.tDelay = 0;            % Delay time [ms]
seqParam.nShots = 2;            % Number of shots before reaching steady state
seqParam.nPartitions = 30;      % Number of partitions
seqParam.seqLength = nFAs;      % Flip angle sequence length
seqParam.normalize = true;      % Normalize the fingerprints in the dictionary
seqParam.averageSets = true;    % Average the signals within each set or not

jacAutodiff = true;             % Use automatic differentiation for the Jacobian

showT1 = 750;       % T1 value for visualization
showB1 = 1;         % Relative B1+ value for visualization

fileName = ['opt_FAs-' num2str(seqParam.seqLength)];

%% Prepare optimization
disp('Set up solver ...')
tic
% Initialize the variables to be optimized
[avgObjFcn, gradFcn, hessMultFcn] = objective(T1List, B1List, m0, seqParam, jacAutodiff);

options = optimoptions('fmincon');
options = optimoptions(options, 'MaxFunctionEvaluations', 30000, 'MaxIterations', 100, 'Display', 'iter');
options = optimoptions(options, 'SpecifyObjectiveGradient', true);
options = optimoptions(options, 'SubproblemAlgorithm', 'cg');
options = optimoptions(options, 'HessianMultiplyFcn', @(x, lambda, v) hessMultFcn(x, v));
toc

%% Optimize flip angles
disp('Run solver ...')
tic

optFAsInterm = zeros(nFAs, nInit);
objOptInterm = zeros(1, nInit);

for i = 1:nInit
    [x, f] = fmincon(@(FAs) objective_fcn(FAs, avgObjFcn, gradFcn), initFAs(:,i), ...
        [], [], [], [], ones(nFAs, 1)*minFA, ones(nFAs, 1)*maxFA, [], options);
    
    optFAsInterm(:,i) = x;
    objOptInterm(i) = f;
end

[objOpt, idx] = min(objOptInterm);
optFAs = optFAsInterm(:,idx);

% Remove sets with a small average flip angle
avgFAs = reshape(optFAs, seqParam.nPartitions, []);
avgFAs = mean(avgFAs);
avgFAs = reshape(repmat(avgFAs, seqParam.nPartitions, 1), [], 1);
optFAs(avgFAs < zeroCutoff) = 0;

toc

%% Show result
fprintf('Final objective: %.4f\n', objOpt)

spTmp = seqParam;
spTmp.averageSets = true;

show_opt_result(showT1, showB1, m0, initFAs(:,1), optFAs, spTmp)

spTmp.averageSets = false;

show_opt_result(showT1, showB1, m0, initFAs(:,1), optFAs, spTmp)

%% Save results
if saveResults == true
    FAs = optFAs*180/pi;
    save(['flip_angles/' fileName '.mat'], 'FAs', 'objOpt')
end

function [avgObj, grad] = objective_fcn(FAs, avgObjFcn, gradFcn)
% Returns the averaged objective value (and optionally its gradient) for a
% given flip angle pattern.

avgObj = avgObjFcn(FAs);

if nargout > 1
    grad = gradFcn(FAs);
end

end
