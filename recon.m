%%-----------------------------------------------------------------------%%
%  Authors: M.A. Cloos and M.H.C. van Riel
%  m.h.c.v.riel@student.tue.nl
%  Date: 2020 May 14
%  New York University School of Medicine, www.cai2r.net
%%-----------------------------------------------------------------------%%
clearvars; close all; clc;
addpath(genpath('./_src'));

%% Settings

% File selection
nFAs = 600;
volunteerNr = 6;
ordering = 'mr';                    % 'mr' for motion-robust ordering
                                    % 'norm' for normal ordering
dataPath = 'Data\';                 % Path to data file
dataFile = sprintf('volunteer%d_%s', volunteerNr, ordering);  % Data file name

FAPath = 'flip_angles/';            % Path to optimized flip angle files
FAFileName = sprintf('opt_FAs-%d', nFAs);    % Optimized flip angle file name

saveData = true;                % Save data or not

% Reconstruction settings
remove_os = true;               % Remove oversampling after reconstruction (by reducing the FOV)
sliceRange = 18;                % Slices to reconstruct
                                % When set to 0: reconstruct all slices
applyMask = false;              % Use the masked images for matching
maskMethod = 'threshold';       % Either 'circles' or 'threshold'
nVirtCoils = 10;                % Number of virtual coils
                                % When set to 0: no coil compression
CSPart = 1/8;                   % Part of k-space to use for coil sensitivity
                                % Lower values result in smoother sensitivity profiles.

plotLevel = 1;                  % 0: Only text
                                % 1: Text and final results
                                % 2: Plot everything

% Dictionary settings
T1Values = 50 * 1.025.^(0:175); % T1 values [ms]
B1Values = 0.02:0.02:2;         % Relative B1+ values [-]
m0 = 1;                         % Equilibrium magnetization (proportional to proton density)
seqParam.TR = 5;                % Repetition time [ms]
seqParam.inversion = true;      % Use inversion pulse or not
seqParam.tInv = 2*seqParam.TR;  % Inversion time [ms]
seqParam.tDelay = 0;            % Delay time [ms]
seqParam.nShots = 2;            % Number of shots before reaching steady state
seqParam.nPartitions = 30;      % Number of partitions
seqParam.seqLength = nFAs;      % Flip angle sequence length
seqParam.normalize = true;      % Normalize the fingerprints in the dictionary
seqParam.svdRange = 5;          % Number of singular values used for dictionary compression
                                % When set to 0: no compression
if strcmp(ordering, 'norm')
    seqParam.averageSets = false;   % Average the signals within each set or not
elseif strcmp(ordering, 'mr')
    seqParam.averageSets = true;
end

% Visualization options
showCoil = 1;                               % For which (virtual) coil to show the sensitivity profiles  
if isequal(sliceRange, 0)                     
    showSlice = 18;                         % For which slice to show the images and parameter maps
else                                        % Select slice 17 by default
    showSlice = find(sliceRange == 18, 1);  
    if isempty(showSlice)
        showSlice = 1;                      % If this slice is not reconstructed, select slice 1 instead
    end
end

%%  Load raw data
disp('Load and preprocess data ...')
tic
MrData = RawDataObj3D(dataPath, [dataFile '.mat']);
MrData.setOrdering(ordering);
MrData.extractNoise(plotLevel);         % Get noise covariance matrix
MrData.maskData();                      % Remove data acquired with very small flip angles
MrData.decorrChannels();                % Decorrelate channels using the noise covariance
MrData.sliceFT();                       % Apply the Fourier Transform in the slice direction
MrData.setSliceRange(sliceRange);       % Select slices
energy = MrData.coilCompression(nVirtCoils, CSPart, plotLevel);  % Apply coil compression
toc

if strcmp(ordering, 'norm') && seqParam.averageSets
    MrData.groupSets(seqParam.nPartitions);
end

dim = MrData.Dim;

%% Remove delay from flip angle pattern

% Load flip angles
load([FAPath, FAFileName, '.mat'], 'FAs')
FAs = FAs * pi / 180;

if seqParam.averageSets
    setSize = seqParam.nPartitions;
else
    setSize = 1;
end

nDelayExc = length(FAs) - dim.nSe * setSize;
seqParam.tDelay = seqParam.tDelay + nDelayExc*seqParam.TR;

FAs = FAs(1:end - nDelayExc);

seqParam.seqLength = length(FAs);

if plotLevel >= 2
    figure, plot(FAs*180/pi, 'LineWidth', 2)
    xlabel('Excitation number'), ylabel('Flip angle (deg)'), title('Flip angle pattern')
end

%% Estimate Recieve Sensitivities 
disp('Estimate receive sensitivities ...')
tic
if nVirtCoils == 1
    I_rx = ones(dim.nSl, dim.nRe, dim.nRe);
else
    I_rx = make_rx(MrData, CSPart);
end
toc

if plotLevel >= 2
    visualize_rx(I_rx, showSlice, 2);
end

%% Load dictionary
disp('Load dictionary ...')
tic
dictionary = create_dictionary(T1Values, B1Values, m0, seqParam, FAs, plotLevel);

% Compress data
if seqParam.svdRange > 0
    MrData.applyCompression(dictionary.U);
end
toc

%% Extract the trajectory & dimenions
disp('Get trajectory ...')
trj = get_trajectory(MrData);
dim = MrData.Dim;

%% Matched filter reconstruction
disp('Reconstruct images ...')
tic
img = reconstruct_mrf(MrData, I_rx, trj);
toc

%% Remove oversampling
if remove_os
    parI = round(size(img,3)/4);
    staI = parI+1;
    endI = round(3*parI);
    img  = img(:,:,staI:endI,staI:endI,:);
end

%% Mask images
mask = create_mask(img, maskMethod, showSlice, plotLevel);

%% Show images
if plotLevel >= 2
    disp('Show images ...')

    if seqParam.svdRange > 0
        setIdx = 1:seqParam.svdRange;
    else
        setIdx = round(linspace(1, dim.nSe, 4));
    end

    show_images(flip(abs(img), 3), flip(mask, 2), setIdx, showSlice)
end

%% Save images
disp('Save images ...')

if saveData == true
    save([dataPath, dataFile, '_images.mat'], 'img', 'mask', 'sliceRange', 'energy')
end

%% Dictionary Matching
disp('Match to dictionary ...')

maps = zeros(dim.nEc, 4, dim.nSl, size(img,3), size(img,4), 'single');
fits = zeros(dim.nEc, dim.nSe, dim.nSl, size(img,3), size(img,4), 'single');

tic
for iEc = 1:dim.nEc
    imgSize = size(img);
    ecImg = reshape(img(iEc,:,:,:,:), imgSize(2:end));
    
    [m, f] = fast_match(ecImg, dictionary, 1000, applyMask, mask);
    
    % Split PD in magnitude and phase.
    % This makes the map variable real instead of complex.
    m(2,:) = angle(m(1,:));
    m(1,:) = abs(m(1,:));
    
    % Remove the fourth row, as this does not store anything.
    m(4,:) = m(5,:);
    m = m(1:4,:,:,:);
    
    maps(iEc,:,:,:,:) = m;
    fits(iEc,:,:,:,:) = abs(f);
end
toc

%% Show parameter maps

if plotLevel >= 1
    disp('Show parameter maps ...')
    ranges = {[0, prctile(maps(1,1,1,:), 98)], [300, 1500], [0, max(B1Values)]};
    show_maps(flip(real(maps), 4), showSlice, ranges)
end

%% Save maps
disp('Saving maps ...')

if saveData == true
    save([dataPath, dataFile, '_maps.mat'], 'maps', 'fits', 'FAs', 'sliceRange')
end
